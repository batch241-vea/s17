/*
	Functions in JavaScript are lines/block of codes that tell our device/application to perform a certain task when called

	Functions are mostly created to create complicated tasks to run several lines of code in succession

	Used to prevent repeating lines or block or codes that perform the same task/function
*/

//Function Declaration
/*
	function statement - defines a function with the specified parameters

	Syntax:
		function functionName() {
			codeblock(statement)
		}

	function keyword - used to define a JS functions
	functionName - are named to be able to use later in the code
	function block ({ }) - the statements which comprise the body of the function. This is where the code to be executed
*/

function printName() {
	console.log("My name is Jenno!");
}

// Function Invocation
/*
	the code block and statements inside the function is not immediately executed when the function is defined. The code block inside a function is executed when the funtion is invoked or called.
*/
//invoke/call the function that we declared
printName();

// declaredFunction(); //error no declaration

//Function Declaration vs Function Expressions

//Function Declaration
	// Function can be created through function declaration by using the function keyword and adding the function name
	// Declared function are NOT executed immediately. They are saved for later used and will be executed later when they are invoked(called).

//Hoisting
declaredFunction();

function declaredFunction(){
	console.log("Hello World from declaredFunction");
}
declaredFunction();

//Function Expression
	// Function can also be stored in a variable. This is called function expression
	// A function expression is an anonymous function assigned to the variableFunction
	// Anonymous function - is a function without a name

let variableFunction = function(){
	console.log("Hello from the variableFunction");
}

variableFunction();

// function expression is always invoke(called) using its variable name
let funcExpression = function funcName(){
	console.log("Hello from the other side");
}

funcExpression();

// can we reaasign a declared functions and function expression to a NEW anonymous functions?

declaredFunction = function(){
	console.log("updated declaredFunction");
}
declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression");
}
funcExpression();

const constantFunction = function(){
	console.log("Initialized");
} 
constantFunction();

/*constantFunction = function(){
	console.log("Initialized");
}
constantFunction();*/

// Function Scoping 
/*
	scope is the accessibility of variables

	JS variables has 3 types of scope
		1. Local/block
		2. Global
		3. Function
*/
{
	let localVar = "I am a local variable";
}
let globalVar = "I am a global variable";

// console.log(localVar);// not defined
console.log(globalVar);

function showNames(){
	//function variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

}
showNames();

// The variables below are function scope variables, meaning they can only accessed inside of the function they were declared in.
	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);


// Nested Function

function myNewFunction(){
	let name = "Maria";

	function nestedFunction(){
		let nestedFunction = "Jose";
		console.log(name);
	}

	nestedFunction();
}

myNewFunction();

// Function and Global Scope variables

	//global variable
	let globalName = "Erven";

	function myNewFunction2(){
		nameInside = "Jenno"; 
		console.log(globalName);
	}
	myNewFunction2();

	// Using Alert
	// alert("Hello World!");

	// function showSampleAlert(){
	// 	alert("Hello World");
	// }

	// showSampleAlert();

	//	
	console.log("Will only log after dismissing the alert!")


	// Using Prompt 
/*	let samplePrompt = prompt("Enter your Name: ");
	
	console.log("Hello, " + samplePrompt);*/
	
/*	function printWelcomeMessage(){
		let firstName1 = prompt("Enter your first name: ");
		let lastName1 = prompt("Enter your last name: ");
		alert("daasd");

		console.log("Hello " + firstName1 + " " + lastName1);
		console.log("Welcome to my page!");
	}

	printWelcomeMessage();*/


	// Function Naming Conventions
		//Name your functions in smalls caps followed by camel case notation when naming variables and functions

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 15,000,000");
	}

	displayCarInfo();

	// function names should be definitive of the task it will perform so it usually contains a verb

	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"]
		console.log(courses);
	}
	getCourses();
	// Avoid generic names to avoid confusion within your code\
	// avoid pointless/inappropriate function names
